import { Component, ViewChild, OnInit } from '@angular/core';
import { BackendApiService } from './services/backend-api.service';
import { Company } from './services/models/company';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'demo';
  displayedColumns: string[] = ['id', 'name', 'federalId', 'naic'];
  dataSource;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private api: BackendApiService) {}

  ngOnInit() {
    this.getCompanyList();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getCompanyList() {
    this.api.getCompanyList().subscribe((data) => {
      if(data) {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.sort;
      }
    });
  }
}
