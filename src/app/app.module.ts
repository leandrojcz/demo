import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import {MatSortModule} from '@angular/material/sort';
import {
  MatButtonModule,
  MatInputModule,
  MatFormFieldModule,
  MatTableModule,
} from '@angular/material';

import { AppComponent } from './app.component';
import { BackendApiService } from './services/backend-api.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSortModule,
    MatInputModule
  ],
  providers: [BackendApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
