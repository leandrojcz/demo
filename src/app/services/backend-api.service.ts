import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Company } from './models/company';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {

  baseUrl: string = 'https://apps.capbpm.com/fit-jpa/'

  constructor(private http: HttpClient) { }

  getCompanyList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/CompanyData/findAll`);
  }

}
