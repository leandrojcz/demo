
export class Company {
  businessUnit?: string;
  companyName?: string;
  federalId?: string;
  id?: number;
  lastUpdateDate?: string;
  lastUpdateId?: string
  naic?: string;
}